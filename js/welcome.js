(function ($) {
  Drupal.behaviors.welcome = {



    attach: function (context, settings) {
            // Code to be run on page load, and
            // on ajax load added here
            // console.log(drupalSettings);


        if ($.cookie('name') !== 'value') {


            var img = "<div id='splashscreen' style='background-image:url(" + drupalSettings.welcome.welcome.testvar1 + "); background-size: cover'>";
            img += '<h2> ' + drupalSettings.welcome.welcome.testvar + "</h2><div id='wcblock' class='center-block'>" + drupalSettings.welcome.welcome.testvar2 + "</div><div class='btn btn-primary enter_link'>ENTER</div>";

            $('body').append(img);



            $('.enter_link').click( function() {
                $.cookie('name', 'value');

                $(this).parent().slideUp(1500);
                $.removecookie('name');

              });
          }


      }
  };
}(jQuery));
