<?php

/**
 * @file
 * Contains \Drupal\welcome\WelcomeManagerInterface.
 */

namespace Drupal\welcome;

/**
 * Provides an interface defining a BLock field manager.
 */
interface WelcomeManagerInterface {

  /**
   * Get sorted listed of supported block definitions.
   *
   * @return array
   *   An associative array of supported block definitions.
   */
  public function getBlockDefinitions();

}
