<?php

namespace Drupal\welcome\Form;

use Drupal\Core\Form\ConfigFormBase;

use Drupal\Core\Form\FormStateInterface;

use Drupal\file\Entity\File;

/**
 * WelcomeForm.
 */
class WelcomeForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'welcome_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'welcome.settings',
    ];
  }


  /**
   * {@inheritdoc}
   */
  protected function blockList() {
    $welcome_manager = \Drupal::service('welcome.manager');
    $definitions = $welcome_manager->getBlockDefinitions();
    foreach ($definitions as $plugin_id => $definition) {
      $options[$definition['provider']][$plugin_id] = $definition['admin_label'];
    }
    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $config = $this->config('welcome.settings');
    $form['welcome'] = array(
      '#type' => 'fieldset',
      '#title' => $this->t('welcome'),
    );

    $form['welcome']['welcome_text'] = array(
      '#type' => 'textarea',
      'title' => $this->t('welcome_text'),
      '#default_value' => $config->get('welcome_text'),
      '#description' => $this->t('to enter your url'),
    );

    $form['welcome']['welcome_image'] = array(
      '#type' => 'managed_file',
      '#title' => $this->t('welcome_image'),
      '#name' => 'files[]',
      '#upload_location' => 'public://',
      '#default_value' => $config->get('welcome_image'),
      '#description' => $this->t('upload the image'),
    );

    $options = $this->blockList();

    $form['welcome']['welcome_block'] = array(
      '#type' => 'select',
      '#title' => $this->t('welcome_block'),
      '#options' => $options,
      '#empty_option' => $this->t('- None -'),
      '#default_value' => $config->get('welcome_block'),
    );
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $image = $form_state->getValue('welcome_image');
    $file = File::load($image[0]);
    if (!empty($file)) {
      $file->setPermanent();
      $file->save();
      $file_usage = \Drupal::service('file.usage');
      $file_usage->add($file, 'welcome', 'welcome', \Drupal::currentUser()->id());
    }
    $config = $this->config('welcome.settings');
    $config->set('welcome_text', $form_state->getValue('welcome_text'))
      ->set('welcome_image', $form_state->getValue('welcome_image'))
      ->set('welcome_block', $form_state->getValue('welcome_block'))
      ->save();
  }

} 

